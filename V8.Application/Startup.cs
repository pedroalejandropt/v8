﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using V8.Domain.Interfaces;
using V8.Domain.Interfaces.EmployeeInterfaces;
using V8.Domain.Interfaces.EmployeeHistoryInterfaces;
using V8.Domain.Interfaces.SalaryInterfaces;
using V8.Domain.Interfaces.OfficeInterfaces;
using V8.Domain.Interfaces.DivisionInterfaces;
using V8.Domain.Interfaces.PositionInterfaces;
using V8.Infra.Data.Repository;
using V8.Service.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace V8.Application
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "V8",
                    Description = "Project Implementing Domain Driven Design",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Pedro Pacheco",
                        Email = "pedro.alejandro.pt@gmail.com",
                        Url = "https://github.com/pedroalejandropt"
                    }
                });
            });

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                options.ReturnHttpNotAcceptable = true;
                options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
            });

            services.AddMvc().AddXmlSerializerFormatters();

            services.AddScoped<IExampleService, ExampleService>();

            services.AddScoped<IExampleRepository, ExampleRepository>();

            services.AddTransient<ExampleService, ExampleService>();

            // Employee

            services.AddScoped<IEmployeeService, EmployeeService>();

            services.AddScoped<IEmployeeRepository, EmployeeRepository>();

            services.AddTransient<EmployeeService, EmployeeService>();

            // Salary

            services.AddScoped<ISalaryService, SalaryService>();

            services.AddScoped<ISalaryRepository, SalaryRepository>();

            services.AddTransient<SalaryService, SalaryService>();

            // EmployeeHistory

            services.AddScoped<IEmployeeHistoryService, EmployeeHistoryService>();

            services.AddScoped<IEmployeeHistoryRepository, EmployeeHistoryRepository>();

            services.AddTransient<EmployeeHistoryService, EmployeeHistoryService>();

            // Office

            services.AddScoped<IOfficeService, OfficeService>();

            services.AddScoped<IOfficeRepository, OfficeRepository>();

            services.AddTransient<OfficeService, OfficeService>();

            // Division

            services.AddScoped<IDivisionService, DivisionService>();

            services.AddScoped<IDivisionRepository, DivisionRepository>();

            services.AddTransient<DivisionService, DivisionService>();

            // Position

            services.AddScoped<IPositionService, PositionService>();

            services.AddScoped<IPositionRepository, PositionRepository>();

            services.AddTransient<PositionService, PositionService>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(MyAllowSpecificOrigins);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvc();
        }
    }
}
