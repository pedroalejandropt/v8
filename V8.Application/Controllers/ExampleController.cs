﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using V8.Domain.Entities;
using V8.Service.Validators;
using V8.Domain.Interfaces;

namespace V8.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExampleController : ControllerBase
    {
        private readonly IExampleService _exampleService;

        public ExampleController(IExampleService exampleService)
        {
            _exampleService = exampleService;
        }

        // GET: api/Example
        [Produces("application/json")]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var examples = _exampleService.Get();
                return Ok(examples);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/Examples/1
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            ExampleEntity example = _exampleService.Get(id);
            if (example == null)
            {
                return NotFound("Example does not exist");
            }
            return Ok(example);
        }

        // PUT: api/Example/1
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] ExampleEntity example)
        {
            try
            {
                _exampleService.Put<ExampleValidator>(example);
                return Ok(example);
            }
            catch (ArgumentNullException)
            {
                return NotFound("Example does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // POST: api/Example
        [HttpPost]
        public IActionResult Post([FromBody] ExampleEntity example)
        {
            try
            {
                _exampleService.Post<ExampleValidator>(example);
                return Ok(example.Id);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // DELETE: api/Example/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _exampleService.Delete(id);
                return Ok();
            }
            catch (ArgumentException)
            {
                return NotFound("Example does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // Get: api/Example/Count
        [Produces("application/xml")]
        [HttpGet("count")]
        public IActionResult GetExamplesAndTotal()
        {
            try
            {
                List<ExampleElementEntity> examples = _exampleService.GetExampleElements();
                return Ok(examples);
            }
            catch (ArgumentException)
            {
                return NotFound("No examples in database");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }      

    }
}