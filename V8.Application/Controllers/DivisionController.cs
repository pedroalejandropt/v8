﻿using System;
using Microsoft.AspNetCore.Mvc;
using V8.Domain.Entities;
using V8.Service.Validators;
using V8.Domain.Interfaces.DivisionInterfaces;

namespace V8.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionController : ControllerBase
    {
        private readonly IDivisionService _divisionService;

        public DivisionController(IDivisionService divisionService)
        {
            _divisionService = divisionService;
        }

        // GET: api/division
        [Produces("application/json")]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var employees = _divisionService.Get();
                return Ok(employees);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/division/1
        [Produces("application/json")]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Division division = _divisionService.Get(id);
            if (division == null)
            {
                return NotFound("Division does not exist");
            }
            return Ok(division);
        }

        // PUT: api/division/1
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Division division)
        {
            try
            {
                _divisionService.Put<DivisionValidator>(division);
                return Ok(division);
            }
            catch (ArgumentNullException)
            {
                return NotFound("Division does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // POST: api/division
        [HttpPost]
        public IActionResult Post([FromBody] Division division)
        {
            try
            {
                _divisionService.Post<DivisionValidator>(division);
                return Ok(division.Id);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // DELETE: api/division/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _divisionService.Delete(id);
                return Ok();
            }
            catch (ArgumentException)
            {
                return NotFound("Division does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
