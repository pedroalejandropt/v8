﻿using System;
using Microsoft.AspNetCore.Mvc;
using V8.Domain.Entities;
using V8.Service.Validators;
using V8.Domain.Interfaces.OfficeInterfaces;

namespace V8.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController : ControllerBase
    {
        private readonly IOfficeService _officeService;

        public OfficeController(IOfficeService officeService)
        {
            _officeService = officeService;
        }

        // GET: api/office
        [Produces("application/json")]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var officess = _officeService.Get();
                return Ok(officess);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/office/1
        [Produces("application/json")]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Office office = _officeService.Get(id);
            if (office == null)
            {
                return NotFound("Office does not exist");
            }
            return Ok(office);
        }

        // PUT: api/office/1
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Office office)
        {
            try
            {
                _officeService.Put<OfficeValidator>(office);
                return Ok(office);
            }
            catch (ArgumentNullException)
            {
                return NotFound("Office does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // POST: api/office
        [HttpPost]
        public IActionResult Post([FromBody] Office office)
        {
            try
            {
                _officeService.Post<OfficeValidator>(office);
                return Ok(office.Id);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // DELETE: api/office/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _officeService.Delete(id);
                return Ok();
            }
            catch (ArgumentException)
            {
                return NotFound("Office does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
