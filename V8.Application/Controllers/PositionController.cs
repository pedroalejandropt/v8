﻿using System;
using Microsoft.AspNetCore.Mvc;
using V8.Domain.Entities;
using V8.Service.Validators;
using V8.Domain.Interfaces.PositionInterfaces;

namespace V8.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
        private readonly IPositionService _positionService;

        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }

        // GET: api/position
        [Produces("application/json")]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var positions = _positionService.Get();
                return Ok(positions);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/position/1
        [Produces("application/json")]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Position position = _positionService.Get(id);
            if (position == null)
            {
                return NotFound("Position does not exist");
            }
            return Ok(position);
        }

        // PUT: api/position/1
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Position position)
        {
            try
            {
                _positionService.Put<PositionValidator>(position);
                return Ok(position);
            }
            catch (ArgumentNullException)
            {
                return NotFound("Position does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // POST: api/position
        [HttpPost]
        public IActionResult Post([FromBody] Position position)
        {
            try
            {
                _positionService.Post<PositionValidator>(position);
                return Ok(position.Id);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // DELETE: api/position/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _positionService.Delete(id);
                return Ok();
            }
            catch (ArgumentException)
            {
                return NotFound("Position does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
