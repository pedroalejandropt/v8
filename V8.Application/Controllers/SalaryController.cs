﻿using System;
using Microsoft.AspNetCore.Mvc;
using V8.Domain.Entities;
using V8.Domain.DTOs;
using V8.Service.Validators;
using V8.Domain.Interfaces.EmployeeInterfaces;
using V8.Domain.Interfaces.SalaryInterfaces;
using V8.Domain.Interfaces.EmployeeHistoryInterfaces;
using V8.Infra.Data.Context;

namespace V8.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : ControllerBase
    {
        private SqlServerContext context = new SqlServerContext();
        private readonly ISalaryService _salaryService;
        private readonly IEmployeeHistoryService _employeeHistoryService;

        public SalaryController(ISalaryService salaryService, IEmployeeHistoryService employeeHistoryService)
        {
            _salaryService = salaryService;
            _employeeHistoryService = employeeHistoryService;
        }

        // GET: api/salary
        [Produces("application/json")]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var salaries = _salaryService.Get();
                return Ok(salaries);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/salary/history
        [Produces("application/json")]
        [HttpGet("history")]
        public IActionResult Get_History()
        {
            try
            {
                var history = _employeeHistoryService.Get();
                return Ok(history);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/salary/last
        [Produces("application/json")]
        [HttpGet("last/{id}")]
        public IActionResult Get_Last_By_Employee(string id)
        {
            try
            {
                var bono = _employeeHistoryService.GetThreeLast(id);
                return Ok(bono);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/salary/1
        [Produces("application/json")]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Salary salary = _salaryService.Get(id);
            if (salary == null)
            {
                return NotFound("Salary does not exist");
            }
            return Ok(salary);
        }

        // PUT: api/salary/1
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Salary salary)
        {
            try
            {
                _salaryService.Put<SalaryValidator>(salary);
                return Ok(salary);
            }
            catch (ArgumentNullException)
            {
                return NotFound("Salary does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // POST: api/salary
        [HttpPost]
        public IActionResult Post([FromBody] EmployeeHistory employeeHistory)
        {
            try
            {
                _salaryService.Post<SalaryValidator>(employeeHistory.Salary);

                EmployeeHistory TempEmployeeHistory = new EmployeeHistory();

                if (employeeHistory.OfficeId == 0)
                {
                    EmployeeHistory TempHistory = _employeeHistoryService.GetLast(employeeHistory.Employee.Id);
                    TempEmployeeHistory.EmployeeId = TempHistory.EmployeeId;
                    TempEmployeeHistory.BeginDate = TempHistory.BeginDate;
                    TempEmployeeHistory.OfficeId = TempHistory.OfficeId;
                    TempEmployeeHistory.DivisionId = TempHistory.DivisionId;
                    TempEmployeeHistory.PositionId = TempHistory.PositionId;
                }
                else
                {
                    TempEmployeeHistory.EmployeeId = employeeHistory.Employee.Id;
                    TempEmployeeHistory.BeginDate = employeeHistory.BeginDate;
                    TempEmployeeHistory.OfficeId = employeeHistory.OfficeId;
                    TempEmployeeHistory.DivisionId = employeeHistory.DivisionId;
                    TempEmployeeHistory.PositionId = employeeHistory.PositionId;
                }

                TempEmployeeHistory.Grade = employeeHistory.Grade;
                TempEmployeeHistory.Month = employeeHistory.Month;
                TempEmployeeHistory.Year = employeeHistory.Year;
                TempEmployeeHistory.SalaryId = employeeHistory.Salary.Id;
                var eh = _employeeHistoryService.Post<EmployeeHistoryValidator>(TempEmployeeHistory);

                return Ok(eh.Id);
            }
            catch (ArgumentNullException ex)
            {
                if (employeeHistory.Salary.Id != 0) _salaryService.Delete(employeeHistory.Salary.Id);
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                if (employeeHistory.Salary.Id != 0) _salaryService.Delete(employeeHistory.Salary.Id);
                return BadRequest(ex);
            }
        }

        // DELETE: api/salary/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _salaryService.Delete(id);
                return Ok();
            }
            catch (ArgumentException)
            {
                return NotFound("Salary does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
