﻿using System;
using Microsoft.AspNetCore.Mvc;
using V8.Domain.Entities;
using V8.Service.Validators;
using V8.Domain.Interfaces.EmployeeInterfaces;

namespace V8.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        // GET: api/employee
        [Produces("application/json")]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var employees = _employeeService.Get();
                return Ok(employees);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/employee/1
        [Produces("application/json")]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Employee employee = _employeeService.Get(id);
            if (employee == null)
            {
                return NotFound("Employee does not exist");
            }
            return Ok(employee);
        }

        // PUT: api/employee/1
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Employee employee)
        {
            try
            {
                _employeeService.Put<EmployeeValidator>(employee);
                return Ok(employee);
            }
            catch (ArgumentNullException)
            {
                return NotFound("Employee does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // POST: api/employee
        [HttpPost]
        public IActionResult Post([FromBody] Employee employee)
        {
            try
            {
                _employeeService.Post<EmployeeValidator>(employee);
                return Ok(employee.Id);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // DELETE: api/employee/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _employeeService.Delete(id);
                return Ok();
            }
            catch (ArgumentException)
            {
                return NotFound("Employee does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
