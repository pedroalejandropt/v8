﻿using System;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Service.Validators
{
    public class DivisionValidator : AbstractValidator<Division>
    {
        public DivisionValidator()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Can't found the object.");
                });

            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Is necessary to inform the Name.")
                .MaximumLength(150).WithMessage("Name must be no longer than 150 characters.")
                .NotNull().WithMessage("Is necessary to inform the Name.");

            RuleFor(c => c.Description)
                .MaximumLength(200).WithMessage("Description must be no longer than 200 characters.");
        }
    }
}
