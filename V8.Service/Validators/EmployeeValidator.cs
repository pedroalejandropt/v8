﻿using System;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Service.Validators
{
    public class EmployeeValidator : AbstractValidator<Employee>
    {
        public EmployeeValidator()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Can't found the object.");
                });

            RuleFor(c => c.IdentificationNumber)
                .NotEmpty().WithMessage("Is necessary to inform the IdentificationNumber.")
                .MaximumLength(10).WithMessage("Name must be no longer than 10 characters.")
                .NotNull().WithMessage("Is necessary to inform the IdentificationNumber.");

            RuleFor(c => c.EmployeeCode)
                .NotEmpty().WithMessage("Is necessary to inform the EmployeeCode.")
                .MaximumLength(10).WithMessage("Name must be no longer than 10 characters.")
                .NotNull().WithMessage("Is necessary to inform the EmployeeCode.");

            RuleFor(c => c.EmployeeName)
                .NotEmpty().WithMessage("Is necessary to inform the EmployeeName.")
                .MaximumLength(150).WithMessage("EmployeeName must be no longer than 150 characters.")
                .NotNull().WithMessage("Is necessary to inform the EmployeeName.");

            RuleFor(c => c.EmployeeSurname)
                .NotEmpty().WithMessage("Is necessary to inform the EmployeeSurname.")
                .MaximumLength(150).WithMessage("EmployeeSurname must be no longer than 150 characters.")
                .NotNull().WithMessage("Is necessary to inform the EmployeeSurname.");

            RuleFor(c => c.Birthday)
                .NotEmpty().WithMessage("Is necessary to inform the Birthday.")
                .NotNull().WithMessage("Is necessary to inform the Birthday.");
        }
    }
}
