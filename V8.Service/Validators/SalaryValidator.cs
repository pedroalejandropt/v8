﻿using System;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Service.Validators
{
    public class SalaryValidator : AbstractValidator<Salary>
    {
        public SalaryValidator()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Can't found the object.");
                });

            RuleFor(c => c.BaseSalary)
                //.NotEmpty().WithMessage("Is necessary to inform the BaseSalary.")
                .NotNull().WithMessage("Is necessary to inform the BaseSalary.");

            RuleFor(c => c.ProductionBonus)
                //.NotEmpty().WithMessage("Is necessary to inform the ProductionBonus.")
                .NotNull().WithMessage("Is necessary to inform the ProductionBonus.");

            RuleFor(c => c.CompensationBonus)
                //.NotEmpty().WithMessage("Is necessary to inform the CompensationBonus.")
                .NotNull().WithMessage("Is necessary to inform the CompensationBonus.");

            RuleFor(c => c.Commision)
                //.NotEmpty().WithMessage("Is necessary to inform the Commision.")
                .NotNull().WithMessage("Is necessary to inform the Commision.");

            RuleFor(c => c.Contributions)
                //.NotEmpty().WithMessage("Is necessary to inform the Contributions.")
                .NotNull().WithMessage("Is necessary to inform the Contributions.");
        }
    }
}
