﻿using System;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Service.Validators
{
    public class EmployeeHistoryValidator : AbstractValidator<EmployeeHistory>
    {
        public EmployeeHistoryValidator()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Can't found the object.");
                });

            RuleFor(c => c.Month)
                .NotEmpty().WithMessage("Is necessary to inform the Month.")
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(12)
                .NotNull().WithMessage("Is necessary to inform the Month.");

            RuleFor(c => c.Year)
                .NotEmpty().WithMessage("Is necessary to inform the Year.")
                .GreaterThanOrEqualTo(1950)
                .LessThanOrEqualTo(3000)
                .NotNull().WithMessage("Is necessary to inform the Year.");

            RuleFor(c => c.Grade)
                .NotEmpty().WithMessage("Is necessary to inform the Grade.")
                .NotNull().WithMessage("Is necessary to inform the Grade.");

            RuleFor(c => c.BeginDate)
                .NotEmpty().WithMessage("Is necessary to inform the BeginDate.")
                .NotNull().WithMessage("Is necessary to inform the BeginDate.");

            RuleFor(c => c.EmployeeId)
                .NotEmpty().WithMessage("Is necessary to inform the EmployeeId.")
                .NotNull().WithMessage("Is necessary to inform the EmployeeId.");

            RuleFor(c => c.OfficeId)
                .NotEmpty().WithMessage("Is necessary to inform the OfficeId.")
                .NotNull().WithMessage("Is necessary to inform the OfficeId.");

            RuleFor(c => c.DivisionId)
                .NotEmpty().WithMessage("Is necessary to inform the DivisionId.")
                .NotNull().WithMessage("Is necessary to inform the DivisionId.");

            RuleFor(c => c.PositionId)
                .NotEmpty().WithMessage("Is necessary to inform the PositionId.")
                .NotNull().WithMessage("Is necessary to inform the PositionId.");
        }
    }
}
