﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Service.Validators
{
    public class ExampleValidator : AbstractValidator<ExampleEntity>
    {
        public ExampleValidator()
        {

            RuleFor(c => c)
                    .NotNull()
                    .OnAnyFailure(x =>
                    {
                        throw new ArgumentNullException("Can't found the object.");
                    });

            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Is necessary to inform the Name.")
                .MaximumLength(50).WithMessage("Name must be no longer than 50 characters.")
                .NotNull().WithMessage("Is necessary to inform the Name.");
        }
    }
}
