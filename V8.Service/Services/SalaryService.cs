﻿using System;
using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.Interfaces.SalaryInterfaces;

namespace V8.Service.Services
{
    public class SalaryService : ISalaryService
    {
        private readonly ISalaryRepository _salaryRepository;

        public SalaryService(ISalaryRepository salaryRepository)
        {
            _salaryRepository = salaryRepository;
        }

        private void Validate(Salary salary, AbstractValidator<Salary> validator)
        {
            if (salary == null)
                throw new Exception("records not found!");

            validator.ValidateAndThrow(salary);
        }

        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            _salaryRepository.Delete(id);
        }

        public Salary Post<V>(Salary salary) where V : AbstractValidator<Salary>
        {
            Validate(salary, Activator.CreateInstance<V>());

            _salaryRepository.Insert(salary);
            return salary;
        }

        public Salary Put<V>(Salary salary) where V : AbstractValidator<Salary>
        {
            Validate(salary, Activator.CreateInstance<V>());

            _salaryRepository.Update(salary);
            return salary;
        }

        public Salary Get(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            return _salaryRepository.Select(id);
        }

        public IList<Salary> Get() => _salaryRepository.SelectSalaries();

    }
}
