﻿using System;
using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.Interfaces.EmployeeInterfaces;

namespace V8.Service.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        private void Validate(Employee employee, AbstractValidator<Employee> validator)
        {
            if (employee == null)
                throw new Exception("records not found!");

            validator.ValidateAndThrow(employee);
        }

        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            _employeeRepository.Delete(id);
        }

        public Employee Post<V>(Employee employee) where V : AbstractValidator<Employee>
        {
            Validate(employee, Activator.CreateInstance<V>());

            _employeeRepository.Insert(employee);
            return employee;
        }

        public Employee Put<V>(Employee employee) where V : AbstractValidator<Employee>
        {
            Validate(employee, Activator.CreateInstance<V>());

            _employeeRepository.Update(employee);
            return employee;
        }

        public Employee Get(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            return _employeeRepository.Select(id);
        }

        public IList<Employee> Get() => _employeeRepository.SelectEmployees();

    }
}
