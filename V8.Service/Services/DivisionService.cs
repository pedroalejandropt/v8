﻿using System;
using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.Interfaces.DivisionInterfaces;

namespace V8.Service.Services
{
    public class DivisionService : IDivisionService
    {
        private readonly IDivisionRepository _divisionRepository;

        public DivisionService(IDivisionRepository divisionRepository)
        {
            _divisionRepository = divisionRepository;
        }

        private void Validate(Division division, AbstractValidator<Division> validator)
        {
            if (division == null)
                throw new Exception("records not found!");

            validator.ValidateAndThrow(division);
        }

        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            _divisionRepository.Delete(id);
        }

        public Division Post<V>(Division division) where V : AbstractValidator<Division>
        {
            Validate(division, Activator.CreateInstance<V>());

            _divisionRepository.Insert(division);
            return division;
        }

        public Division Put<V>(Division division) where V : AbstractValidator<Division>
        {
            Validate(division, Activator.CreateInstance<V>());

            _divisionRepository.Update(division);
            return division;
        }

        public Division Get(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            return _divisionRepository.Select(id);
        }

        public IList<Division> Get() => _divisionRepository.SelectDivisions();

    }
}
