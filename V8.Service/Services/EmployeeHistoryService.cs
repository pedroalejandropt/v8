﻿using System;
using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.DTOs;
using V8.Domain.Interfaces.EmployeeHistoryInterfaces;

namespace V8.Service.Services
{
    public class EmployeeHistoryService : IEmployeeHistoryService
    {
        private readonly IEmployeeHistoryRepository _employeeHistoryRepository;

        public EmployeeHistoryService(IEmployeeHistoryRepository employeeHistoryRepository)
        {
            _employeeHistoryRepository = employeeHistoryRepository;
        }

        private void Validate(EmployeeHistory employeeHistory, AbstractValidator<EmployeeHistory> validator)
        {
            if (employeeHistory == null)
                throw new Exception("records not found!");

            validator.ValidateAndThrow(employeeHistory);
        }

        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            _employeeHistoryRepository.Delete(id);
        }

        public EmployeeHistory Post<V>(EmployeeHistory employeeHistory) where V : AbstractValidator<EmployeeHistory>
        {
            Validate(employeeHistory, Activator.CreateInstance<V>());

            _employeeHistoryRepository.Insert(employeeHistory);
            return employeeHistory;
        }

        public EmployeeHistory Put<V>(EmployeeHistory employeeHistory) where V : AbstractValidator<EmployeeHistory>
        {
            Validate(employeeHistory, Activator.CreateInstance<V>());

            _employeeHistoryRepository.Update(employeeHistory);
            return employeeHistory;
        }

        public EmployeeHistory Get(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            return _employeeHistoryRepository.Select(id);
        }

        public EmployeeHistory GetLast(int employeeId)
        {
            if (employeeId == 0)
                throw new ArgumentException("The id can't be zero.");

            return _employeeHistoryRepository.SelectLast(employeeId);
        }

        public decimal GetThreeLast(string employeeId)
        {
            if (employeeId == "")
                throw new ArgumentException("The id can't be zero.");

            return _employeeHistoryRepository.SelectThreeLast(employeeId);
        }

        public IList<EmployeeHistoryDTO> Get() => _employeeHistoryRepository.SelectEmployeeHistories();

    }
}
