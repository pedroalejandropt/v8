﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.Interfaces;
using V8.Infra.Data.Repository;


namespace V8.Service.Services
{
    public class ExampleService : IExampleService
    {
        private readonly IExampleRepository _exampleRepository;

        public ExampleService(IExampleRepository exampleRepository)
        {
            _exampleRepository = exampleRepository;
        }

        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            _exampleRepository.Delete(id);
        }

        public ExampleEntity Get(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            return _exampleRepository.Select(id);
        }

        public IList<ExampleEntity> Get() => _exampleRepository.SelectExampleEntities();

        public ExampleEntity Post<V>(ExampleEntity obj) where V : AbstractValidator<ExampleEntity>
        {
            Validate(obj, Activator.CreateInstance<V>());

            _exampleRepository.Insert(obj);
            return obj;
        }

        public ExampleEntity Put<V>(ExampleEntity obj) where V : AbstractValidator<ExampleEntity>
        {
            Validate(obj, Activator.CreateInstance<V>());

            _exampleRepository.Update(obj);
            return obj;
        }

        public List<ExampleElementEntity> GetExamplesAndTotal() => _exampleRepository.SelectExampleEntitiesWithTotal();

        private void Validate(ExampleEntity obj, AbstractValidator<ExampleEntity> validator)
        {
            if (obj == null)
                throw new Exception("records not found!");

            validator.ValidateAndThrow(obj);
        }

        public List<ExampleElementEntity> GetExampleElements()
        {
            throw new NotImplementedException();
        }
    }
}
