﻿using System;
using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.Interfaces.PositionInterfaces;

namespace V8.Service.Services
{
    public class PositionService : IPositionService
    {
        private readonly IPositionRepository _positionRepository;

        public PositionService(IPositionRepository positionRepository)
        {
            _positionRepository = positionRepository;
        }

        private void Validate(Position position, AbstractValidator<Position> validator)
        {
            if (position == null)
                throw new Exception("records not found!");

            validator.ValidateAndThrow(position);
        }

        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            _positionRepository.Delete(id);
        }

        public Position Post<V>(Position position) where V : AbstractValidator<Position>
        {
            Validate(position, Activator.CreateInstance<V>());

            _positionRepository.Insert(position);
            return position;
        }

        public Position Put<V>(Position position) where V : AbstractValidator<Position>
        {
            Validate(position, Activator.CreateInstance<V>());

            _positionRepository.Update(position);
            return position;
        }

        public Position Get(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            return _positionRepository.Select(id);
        }

        public IList<Position> Get() => _positionRepository.SelectPositions();

    }
}
