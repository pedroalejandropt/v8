﻿using System;
using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.Interfaces.OfficeInterfaces;

namespace V8.Service.Services
{
    public class OfficeService : IOfficeService
    {
        private readonly IOfficeRepository _officeRepository;

        public OfficeService(IOfficeRepository officeRepository)
        {
            _officeRepository = officeRepository;
        }

        private void Validate(Office office, AbstractValidator<Office> validator)
        {
            if (office == null)
                throw new Exception("records not found!");

            validator.ValidateAndThrow(office);
        }

        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            _officeRepository.Delete(id);
        }

        public Office Post<V>(Office office) where V : AbstractValidator<Office>
        {
            Validate(office, Activator.CreateInstance<V>());

            _officeRepository.Insert(office);
            return office;
        }

        public Office Put<V>(Office office) where V : AbstractValidator<Office>
        {
            Validate(office, Activator.CreateInstance<V>());

            _officeRepository.Update(office);
            return office;
        }

        public Office Get(int id)
        {
            if (id == 0)
                throw new ArgumentException("The id can't be zero.");

            return _officeRepository.Select(id);
        }

        public IList<Office> Get() => _officeRepository.SelectOffices();

    }
}
