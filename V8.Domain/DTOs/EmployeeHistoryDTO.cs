﻿using System;
using V8.Domain.Entities;

namespace V8.Domain.DTOs
{
    public class EmployeeHistoryDTO
    {
        public int Grade { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime BeginDate { get; set; }

        public string IdentificationNumber { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public DateTime Birthday { get; set; }

        //public Employee Employee { get; set; }

        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commision { get; set; }
        public decimal Contributions { get; set; }
        public string TotalSalary { get; set; }

        //public Salary Salary { get; set; }

        public string OfficeName { get; set; }
        public string DivisionName { get; set; }
        public string PositionName { get; set; }

        //public Office Office { get; set; }
        //public Division Division { get; set; }
        //public Position Position { get; set; }
    }
}
