﻿namespace V8.Domain.Entities
{
    public class Position : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
