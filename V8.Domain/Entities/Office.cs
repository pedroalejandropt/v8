﻿namespace V8.Domain.Entities
{
    public class Office : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
