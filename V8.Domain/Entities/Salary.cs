﻿namespace V8.Domain.Entities
{
    public class Salary : BaseEntity
    {
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commision { get; set; }
        public decimal Contributions { get; set; }
    }
}
