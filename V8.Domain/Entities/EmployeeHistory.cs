﻿using System;

namespace V8.Domain.Entities
{
    public class EmployeeHistory : BaseEntity
    {
        public int Grade { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime BeginDate { get; set; }
        // FKs
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public int SalaryId { get; set; }
        public virtual Salary Salary { get; set; }
        public int OfficeId { get; set; }
        public virtual Office Office { get; set; }
        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }
        public int PositionId { get; set; }
        public virtual Position Position { get; set; }
    }
}
