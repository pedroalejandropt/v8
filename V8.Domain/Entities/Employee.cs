﻿using System;

namespace V8.Domain.Entities
{
    public class Employee : BaseEntity
    {
        public string IdentificationNumber { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public DateTime Birthday { get; set; }
    }
}
