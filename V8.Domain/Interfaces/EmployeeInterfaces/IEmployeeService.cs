﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.EmployeeInterfaces
{
    public interface IEmployeeService
    {
        Employee Post<V>(Employee obj) where V : AbstractValidator<Employee>;

        Employee Put<V>(Employee obj) where V : AbstractValidator<Employee>;

        void Delete(int id);

        Employee Get(int id);

        IList<Employee> Get();
    }
}
