﻿using System.Collections.Generic;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.EmployeeInterfaces
{
    public interface IEmployeeRepository
    {
        void Insert(Employee obj);

        void Update(Employee obj);

        void Delete(int id);

        Employee Select(int id);

        IList<Employee> SelectEmployees();
    }
}
