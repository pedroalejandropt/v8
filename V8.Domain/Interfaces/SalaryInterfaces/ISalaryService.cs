﻿using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.SalaryInterfaces
{
    public interface ISalaryService
    {
        Salary Post<V>(Salary obj) where V : AbstractValidator<Salary>;

        Salary Put<V>(Salary obj) where V : AbstractValidator<Salary>;

        void Delete(int id);

        Salary Get(int id);

        IList<Salary> Get();
    }
}
