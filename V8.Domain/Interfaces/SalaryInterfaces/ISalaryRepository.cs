﻿using System.Collections.Generic;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.SalaryInterfaces
{
    public interface ISalaryRepository
    {
        void Insert(Salary obj);

        void Update(Salary obj);

        void Delete(int id);

        Salary Select(int id);

        IList<Salary> SelectSalaries();
    }
}