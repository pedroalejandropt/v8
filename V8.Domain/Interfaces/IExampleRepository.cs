﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces
{
    public interface IExampleRepository
    {
        void Insert(ExampleEntity obj);
        
        void Update(ExampleEntity obj);

        void Delete(int id);

        ExampleEntity Select(int id);

        IList<ExampleEntity> SelectExampleEntities();

        List<ExampleElementEntity> SelectExampleEntitiesWithTotal();
    }
}
