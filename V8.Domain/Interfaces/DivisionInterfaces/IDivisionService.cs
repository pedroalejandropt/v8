﻿using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.DivisionInterfaces
{
    public interface IDivisionService
    {
        Division Post<V>(Division obj) where V : AbstractValidator<Division>;

        Division Put<V>(Division obj) where V : AbstractValidator<Division>;

        void Delete(int id);

        Division Get(int id);

        IList<Division> Get();
    }
}
