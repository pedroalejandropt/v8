﻿using System.Collections.Generic;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.DivisionInterfaces
{
    public interface IDivisionRepository
    {
        void Insert(Division obj);

        void Update(Division obj);

        void Delete(int id);

        Division Select(int id);

        IList<Division> SelectDivisions();
    }
}