﻿using System.Collections.Generic;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.PositionInterfaces
{
    public interface IPositionRepository
    {
        void Insert(Position obj);

        void Update(Position obj);

        void Delete(int id);

        Position Select(int id);

        IList<Position> SelectPositions();
    }
}