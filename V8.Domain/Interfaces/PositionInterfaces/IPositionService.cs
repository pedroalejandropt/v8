﻿using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.PositionInterfaces
{
    public interface IPositionService
    {
        Position Post<V>(Position obj) where V : AbstractValidator<Position>;

        Position Put<V>(Position obj) where V : AbstractValidator<Position>;

        void Delete(int id);

        Position Get(int id);

        IList<Position> Get();
    }
}
