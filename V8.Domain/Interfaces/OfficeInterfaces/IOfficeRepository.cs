﻿using System.Collections.Generic;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.OfficeInterfaces
{
    public interface IOfficeRepository
    {
        void Insert(Office obj);

        void Update(Office obj);

        void Delete(int id);

        Office Select(int id);

        IList<Office> SelectOffices();
    }
}
