﻿using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.OfficeInterfaces
{
    public interface IOfficeService
    {
        Office Post<V>(Office obj) where V : AbstractValidator<Office>;

        Office Put<V>(Office obj) where V : AbstractValidator<Office>;

        void Delete(int id);

        Office Get(int id);

        IList<Office> Get();
    }
}
