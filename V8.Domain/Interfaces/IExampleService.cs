﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentValidation;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces
{
    public interface IExampleService
    {
        ExampleEntity Post<V>(ExampleEntity obj) where V : AbstractValidator<ExampleEntity>;

        ExampleEntity Put<V>(ExampleEntity obj) where V : AbstractValidator<ExampleEntity>;

        void Delete(int id);

        ExampleEntity Get(int id);

        IList<ExampleEntity> Get();

        List<ExampleElementEntity> GetExampleElements();
    }
}
