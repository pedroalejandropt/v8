﻿using System.Collections.Generic;
using V8.Domain.Entities;
using V8.Domain.DTOs;

namespace V8.Domain.Interfaces.EmployeeHistoryInterfaces
{
    public interface IEmployeeHistoryRepository
    {
        void Insert(EmployeeHistory obj);

        void Update(EmployeeHistory obj);

        void Delete(int id);

        EmployeeHistory Select(int id);

        EmployeeHistory SelectLast(int employeeId);

        decimal SelectThreeLast(string employeeId);

        IList<EmployeeHistoryDTO> SelectEmployeeHistories();
    }
}