﻿using System.Collections.Generic;
using FluentValidation;
using V8.Domain.Entities;
using V8.Domain.DTOs;

namespace V8.Domain.Interfaces.EmployeeHistoryInterfaces
{
    public interface IEmployeeHistoryService
    {
        EmployeeHistory Post<V>(EmployeeHistory obj) where V : AbstractValidator<EmployeeHistory>;

        EmployeeHistory Put<V>(EmployeeHistory obj) where V : AbstractValidator<EmployeeHistory>;

        void Delete(int id);

        EmployeeHistory Get(int id);

        EmployeeHistory GetLast(int employeeId);

        decimal GetThreeLast(string employeeId);

        IList<EmployeeHistoryDTO> Get();
    }
}
