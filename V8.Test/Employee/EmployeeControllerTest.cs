﻿using System;
using Xunit;
using V8.Application.Controllers;
using V8.Domain.Entities;
using V8.Infra.Data.Repository;
using V8.Service.Services;
using Microsoft.AspNetCore.Mvc;

namespace V8.Test.EmployeeTest
{
    public class EmployeeControllerTest
    {
        [Fact]
        public void GetAll()
        {
            //Arrange the resources
            var repo = new EmployeeRepository();
            var service = new EmployeeService(repo);
            var controller = new EmployeeController(service);

            //Act on the functionality
            IActionResult response = controller.Get();
            var result = response as OkObjectResult;

            //Assert
            Assert.NotNull(response);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public void GetBy()
        {
            //Arrange the resources
            var repo = new EmployeeRepository();
            var service = new EmployeeService(repo);
            var controller = new EmployeeController(service);

            //Act on the functionality
            IActionResult response = controller.Get(1);
            var result = response as OkObjectResult;

            //Assert
            Assert.NotNull(response);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public void Post()
        {
            //Arrange the resources
            var repo = new EmployeeRepository();
            var service = new EmployeeService(repo);
            var controller = new EmployeeController(service);

            var employee = new Employee()
            {
                IdentificationNumber = "987654321",
                EmployeeCode = "123456789",
                EmployeeName = "Pedro",
                EmployeeSurname = "Pacheco",
                Birthday = DateTime.Now
            };

            //Act on the functionality
            IActionResult response = controller.Post(employee);
            var result = response as OkObjectResult;

            //Assert
            Assert.NotNull(response);
            Assert.Equal(200, result.StatusCode);
        }
    }
}
