﻿using System;
using Xunit;
using V8.Application.Controllers;
using V8.Domain.Entities;
using V8.Infra.Data.Repository;
using V8.Service.Services;
using Microsoft.AspNetCore.Mvc;

namespace V8.Test.SalaryTest
{
    public class SalaryControllerTest
    {
        [Fact]
        public void GetHistory()
        {
            //Arrange the resources
            var repo = new SalaryRepository();
            var repoHistory = new EmployeeHistoryRepository();
            var service = new SalaryService(repo);
            var serviceHistory = new EmployeeHistoryService(repoHistory);
            var controller = new SalaryController(service, serviceHistory);

            //Act on the functionality
            IActionResult response = controller.Get_History();
            var result = response as OkObjectResult;

            //Assert
            Assert.NotNull(response);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public void GetBono()
        {
            //Arrange the resources
            var repo = new SalaryRepository();
            var repoHistory = new EmployeeHistoryRepository();
            var service = new SalaryService(repo);
            var serviceHistory = new EmployeeHistoryService(repoHistory);
            var controller = new SalaryController(service, serviceHistory);

            //Act on the functionality
            IActionResult response = controller.Get_Last_By_Employee("10001222");
            var result = response as OkObjectResult;

            //Assert
            Assert.NotNull(response);
            Assert.Equal(200, result.StatusCode);
        }
    }
}
