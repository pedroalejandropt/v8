﻿using System.Collections.Generic;
using V8.Domain.Entities;
using V8.Domain.Interfaces;
using V8.Infra.Data.Context;
using System.Linq;

namespace V8.Infra.Data.Repository
{
    public class ExampleRepository : IExampleRepository
    {
        private SqlServerContext context = new SqlServerContext();

        public void Insert(ExampleEntity example)
        {
            context.Set<ExampleEntity>().Add(example);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<ExampleEntity>().Remove(Select(id));
            context.SaveChanges();
        }
             
        public ExampleEntity Select(int id)
        {
            return context.Set<ExampleEntity>().Find(id);
        }

        public IList<ExampleEntity> SelectExampleEntities()
        {
            return context.Set<ExampleEntity>().ToList();
        }

        public void Update(ExampleEntity obj)
        {
            context.Entry(obj).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public List<ExampleElementEntity> SelectExampleEntitiesWithTotal()
        {
            return (from b in context.Set<ExampleEntity>()
                    select new ExampleElementEntity
                    {
                        Id = b.Id,
                        Name = b.Name,
                        TotalValueExample = 5
                    }).ToList() ; 
        }

    }
}
