﻿using System.Linq;
using V8.Domain.Entities;
using V8.Infra.Data.Context;
using System.Collections.Generic;
using V8.Domain.Interfaces.PositionInterfaces;

namespace V8.Infra.Data.Repository
{
    public class PositionRepository : IPositionRepository
    {
        private SqlServerContext context = new SqlServerContext();

        public void Insert(Position position)
        {
            context.Set<Position>().Add(position);
            context.SaveChanges();
        }

        public void Update(Position position)
        {
            context.Entry(position).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<Position>().Remove(Select(id));
            context.SaveChanges();
        }

        public Position Select(int id)
        {
            return context.Set<Position>().Find(id);
        }

        public IList<Position> SelectPositions()
        {
            return context.Set<Position>().ToList();
        }
    }
}
