﻿using System.Collections.Generic;
using V8.Domain.Entities;
using V8.Domain.Interfaces.EmployeeHistoryInterfaces;
using V8.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using V8.Domain.DTOs;

namespace V8.Infra.Data.Repository
{
    public class EmployeeHistoryRepository : IEmployeeHistoryRepository
    {
        private SqlServerContext context = new SqlServerContext();

        public void Insert(EmployeeHistory employeeHistory)
        {
            context.Set<EmployeeHistory>().Add(employeeHistory);
            context.SaveChanges();
        }

        public void Update(EmployeeHistory employeeHistory)
        {
            context.Entry(employeeHistory).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<EmployeeHistory>().Remove(Select(id));
            context.SaveChanges();
        }

        public EmployeeHistory Select(int id)
        {
            return context.Set<EmployeeHistory>().Find(id);
        }

        public EmployeeHistory SelectLast(int employeeId)
        {
            return context.Set<EmployeeHistory>()
                .Where(e => e.EmployeeId == employeeId)
                .OrderByDescending(e => e.Year)
                .OrderByDescending(e => e.Month)
                .FirstOrDefault();
        }

        public decimal SelectThreeLast(string employeeId)
        {
            IList<EmployeeHistory> ThreeLast = context.Set<EmployeeHistory>()
                .Include(m => m.Office)
                .Include(m => m.Division)
                .Include(m => m.Position)
                .Include(m => m.Salary)
                .Include(m => m.Employee)
                .Where(e => e.Employee.EmployeeCode == employeeId)
                .OrderByDescending(e => e.Year)
                .OrderByDescending(e => e.Month)
                .Take(3).ToList();

            decimal bono = 0;

            if (ThreeLast.Count == 3)
            {
                if (ThreeLast[0].Month - ThreeLast[1].Month == 1)
                {
                    bono = ThreeLast[0].Salary.BaseSalary + ThreeLast[1].Salary.BaseSalary;
                    if (ThreeLast[1].Month - ThreeLast[2].Month == 1)
                    {
                        bono = bono + ThreeLast[2].Salary.BaseSalary;
                    }
                }
                else
                {
                    bono = ThreeLast[0].Salary.BaseSalary;
                }
            }
            else if(ThreeLast.Count == 2)
            {
                if (ThreeLast[0].Month - ThreeLast[1].Month == 1)
                {
                    bono = ThreeLast[0].Salary.BaseSalary + ThreeLast[1].Salary.BaseSalary;
                }
                else
                {
                    bono = ThreeLast[0].Salary.BaseSalary;
                }
            }
            else
            {
                bono = ThreeLast[0].Salary.BaseSalary;
            }

            bono = bono / 3;

            return bono;

        }

        public IList<EmployeeHistoryDTO> SelectEmployeeHistories()
        {
            var history = context.Set<EmployeeHistory>()
                .Include(m => m.Office)
                .Include(m => m.Division)
                .Include(m => m.Position)
                .Include(m => m.Salary)
                .Include(m => m.Employee)
                .ToList();

            IList<EmployeeHistoryDTO> histories = new List<EmployeeHistoryDTO>();

            history.ForEach(h =>
            {
                decimal OtherIncome = ((h.Salary.BaseSalary + h.Salary.Commision) * ((decimal)0.08)) + h.Salary.Commision;
                decimal TotalSalary = h.Salary.BaseSalary + h.Salary.ProductionBonus + (h.Salary.CompensationBonus * ((decimal)0.075)) + OtherIncome - h.Salary.Contributions;

                var result = new EmployeeHistoryDTO()
                {
                    Grade = h.Grade,
                    Month = h.Month,
                    Year = h.Year,
                    BeginDate = h.BeginDate,
                    IdentificationNumber = h.Employee.IdentificationNumber,
                    EmployeeCode = h.Employee.EmployeeCode,
                    EmployeeName = h.Employee.EmployeeName + ' ' + h.Employee.EmployeeSurname,
                    Birthday = h.Employee.Birthday,
                    BaseSalary = h.Salary.BaseSalary,
                    ProductionBonus = h.Salary.ProductionBonus,
                    CompensationBonus = h.Salary.CompensationBonus,
                    Commision = h.Salary.Commision,
                    Contributions = h.Salary.Contributions,
                    TotalSalary = TotalSalary.ToString("0.##"),
                    OfficeName = h.Office.Name,
                    DivisionName = h.Division.Name,
                    PositionName = h.Position.Name
                };

                histories.Add(result);
            });

            return histories;
            
        }

    }
}
