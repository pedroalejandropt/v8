﻿using System.Linq;
using V8.Domain.Entities;
using V8.Infra.Data.Context;
using System.Collections.Generic;
using V8.Domain.Interfaces.DivisionInterfaces;

namespace V8.Infra.Data.Repository
{
    public class DivisionRepository : IDivisionRepository
    {
        private SqlServerContext context = new SqlServerContext();

        public void Insert(Division division)
        {
            context.Set<Division>().Add(division);
            context.SaveChanges();
        }

        public void Update(Division division)
        {
            context.Entry(division).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<Division>().Remove(Select(id));
            context.SaveChanges();
        }

        public Division Select(int id)
        {
            return context.Set<Division>().Find(id);
        }

        public IList<Division> SelectDivisions()
        {
            return context.Set<Division>().ToList();
        }
    }
}
