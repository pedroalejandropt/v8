﻿using System.Linq;
using V8.Domain.Entities;
using V8.Infra.Data.Context;
using System.Collections.Generic;
using V8.Domain.Interfaces.OfficeInterfaces;

namespace V8.Infra.Data.Repository
{
    public class OfficeRepository : IOfficeRepository
    {
        private SqlServerContext context = new SqlServerContext();

        public void Insert(Office office)
        {
            context.Set<Office>().Add(office);
            context.SaveChanges();
        }

        public void Update(Office office)
        {
            context.Entry(office).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<Office>().Remove(Select(id));
            context.SaveChanges();
        }

        public Office Select(int id)
        {
            return context.Set<Office>().Find(id);
        }

        public IList<Office> SelectOffices()
        {
            return context.Set<Office>().ToList();
        }
    }
}
