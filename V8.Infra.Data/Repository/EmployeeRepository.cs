﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;
using V8.Domain.Interfaces.EmployeeInterfaces;
using V8.Infra.Data.Context;
using System.Linq;
using System.Xml.Linq;

namespace V8.Infra.Data.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private SqlServerContext context = new SqlServerContext();

        public void Insert(Employee employee)
        {
            context.Set<Employee>().Add(employee);
            context.SaveChanges();
        }

        public void Update(Employee employee)
        {
            context.Entry(employee).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<Employee>().Remove(Select(id));
            context.SaveChanges();
        }

        public Employee Select(int id)
        {
            return context.Set<Employee>().Find(id);
        }

        public IList<Employee> SelectEmployees()
        {
            return context.Set<Employee>().ToList();
        }

    }
}
