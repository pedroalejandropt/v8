﻿using System.Linq;
using V8.Domain.Entities;
using V8.Infra.Data.Context;
using System.Collections.Generic;
using V8.Domain.Interfaces.SalaryInterfaces;

namespace V8.Infra.Data.Repository
{
    public class SalaryRepository : ISalaryRepository
    {
        private SqlServerContext context = new SqlServerContext();

        public void Insert(Salary salary)
        {
            context.Set<Salary>().Add(salary);
            context.SaveChanges();
        }

        public void Update(Salary salary)
        {
            context.Entry(salary).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<Salary>().Remove(Select(id));
            context.SaveChanges();
        }

        public Salary Select(int id)
        {
            return context.Set<Salary>().Find(id);
        }

        public IList<Salary> SelectSalaries()
        {
            return context.Set<Salary>().ToList();
        }
    }
}
