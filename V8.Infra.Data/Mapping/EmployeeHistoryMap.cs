﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using V8.Domain.Entities;

namespace V8.Infra.Data.Mapping
{
    public class EmployeeHistoryMap : IEntityTypeConfiguration<EmployeeHistory>
    {
        public void Configure(EntityTypeBuilder<EmployeeHistory> builder)
        {
            builder.ToTable("employee_history");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Month)
                .IsRequired()
                .HasColumnName("Month")
                .HasColumnType("int");

            builder.Property(c => c.Year)
                .IsRequired()
                .HasColumnName("Year")
                .HasColumnType("int");

            builder.Property(c => c.Grade)
                .IsRequired()
                .HasColumnName("Grade")
                .HasColumnType("int");

            builder.Property(c => c.BeginDate)
                .IsRequired()
                .HasColumnName("BeginDate")
                .HasColumnType("datetime");

            builder.HasOne(x => x.Employee).WithMany().HasForeignKey(x => x.EmployeeId).HasConstraintName("employee_fk");
            builder.HasOne(x => x.Salary).WithMany().HasForeignKey(x => x.SalaryId).HasConstraintName("salary_fk");
            builder.HasOne(x => x.Office).WithMany().HasForeignKey(x => x.OfficeId).HasConstraintName("office_fk");
            builder.HasOne(x => x.Division).WithMany().HasForeignKey(x => x.DivisionId).HasConstraintName("division_fk");
            builder.HasOne(x => x.Position).WithMany().HasForeignKey(x => x.PositionId).HasConstraintName("position_fk");
        }
    }
}
