﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using V8.Domain.Entities;

namespace V8.Infra.Data.Mapping
{
    public class SalaryMap : IEntityTypeConfiguration<Salary>
    {
        public void Configure(EntityTypeBuilder<Salary> builder)
        {
            builder.ToTable("salary");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.BaseSalary)
                .IsRequired()
                .HasColumnName("BaseSalary")
                .HasColumnType("float");

            builder.Property(c => c.ProductionBonus)
                .IsRequired()
                .HasColumnName("ProductionBonus")
                .HasColumnType("float");

            builder.Property(c => c.CompensationBonus)
                .IsRequired()
                .HasColumnName("CompensationBonus")
                .HasColumnType("float");

            builder.Property(c => c.Commision)
                .IsRequired()
                .HasColumnName("Commision")
                .HasColumnType("float");

            builder.Property(c => c.Contributions)
                .IsRequired()
                .HasColumnName("Contributions")
                .HasColumnType("float");
        }
    }
}
