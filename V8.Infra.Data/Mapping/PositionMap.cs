﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using V8.Domain.Entities;

namespace V8.Infra.Data.Mapping
{
    public class PositionMap : IEntityTypeConfiguration<Position>
    {
        public void Configure(EntityTypeBuilder<Position> builder)
        {
            builder.ToTable("position");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasColumnType("varchar(150)");

            builder.Property(c => c.Description)
                .IsRequired()
                .HasColumnName("Description")
                .HasColumnType("varchar(200)");
        }
    }
}
