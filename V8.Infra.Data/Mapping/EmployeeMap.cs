﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using V8.Domain.Entities;

namespace V8.Infra.Data.Mapping
{
    public class EmployeeMap : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("employee");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.IdentificationNumber)
                .IsRequired()
                .HasColumnName("IdentificationNumber")
                .HasColumnType("varchar(10)");

            builder.Property(c => c.EmployeeCode)
                .IsRequired()
                .HasColumnName("EmployeeCode")
                .HasColumnType("varchar(10)");

            builder.Property(c => c.EmployeeName)
                .IsRequired()
                .HasColumnName("EmployeeName")
                .HasColumnType("varchar(150)");

            builder.Property(c => c.EmployeeSurname)
                .IsRequired()
                .HasColumnName("EmployeeSurname")
                .HasColumnType("varchar(150)");

            builder.Property(c => c.Birthday)
                .IsRequired()
                .HasColumnName("Birthday")
                .HasColumnType("datetime");

        }
    }
}
