﻿using System;
using System.Collections.Generic;
using V8.Domain.Entities;
using V8.Infra.Data.Context;

namespace V8.Infra.Data.SalaryCreate
{
    public class CreateData
    {
        public static void Execute(SqlServerContext context)
        {
            IList<Salary> salaries = new List<Salary>();

            for (int i = 0; i < 100; i++)
            {
                var salary = new Salary()
                {
                    BaseSalary = Random(),
                    ProductionBonus = Random(),
                    CompensationBonus = Random(),
                    Commision = Random(),
                    Contributions = Random(),
                };

                salaries.Add(salary);
            }

            foreach (Salary e in salaries)
            {
                context.Set<Salary>().Add(e);
            }

            context.SaveChanges();
        }

        public static decimal Random()
        {
            var rand = new Random();
            return ((decimal)(rand.NextDouble() * Math.Abs(3000 - 0)));
        }
    }
}