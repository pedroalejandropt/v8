﻿using System;
using V8.Domain.Entities;
using V8.Infra.Data.Context;

namespace V8.Infra.Data.OfficeCreate
{
    public class CreateData
    {
        public static void Execute(SqlServerContext context)
        {
            var offices = new Office[]
            {
                new Office()
                {
                    Name = "A",
                    Description = ""
                },
                new Office()
                {
                    Name = "B",
                    Description = ""
                },
                new Office()
                {
                    Name = "C",
                    Description = ""
                },
                new Office()
                {
                    Name = "D",
                    Description = ""
                },
                new Office()
                {
                    Name = "E",
                    Description = ""
                },
            };

            foreach (Office e in offices)
            {
                context.Set<Office>().Add(e);
            }

            context.SaveChanges();
        }
    }
}