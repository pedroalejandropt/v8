﻿using System;
using V8.Domain.Entities;
using V8.Infra.Data.Context;

namespace V8.Infra.Data.PositionCreate
{
    public class CreateData
    {
        public static void Execute(SqlServerContext context)
        {
            var positions = new Position[]
            {
                new Position()
                {
                    Name = "Cargo Manager",
                    Description = ""
                },
                new Position()
                {
                    Name = "Head of Cargo",
                    Description = ""
                },
                new Position()
                {
                    Name = "Cargo Assistant",
                    Description = ""
                },
                new Position()
                {
                    Name = "Sales Manager",
                    Description = ""
                },
                new Position()
                {
                    Name = "Account Executive",
                    Description = ""
                },
                new Position()
                {
                    Name = "Marketing Assistant",
                    Description = ""
                },
                new Position()
                {
                    Name = "Customer Director",
                    Description = ""
                },
                new Position()
                {
                    Name = "Customer Assistant",
                    Description = ""
                }
            };

            foreach (Position e in positions)
            {
                context.Set<Position>().Add(e);
            }

            context.SaveChanges();
        }
    }
}