﻿using System;
using System.Collections.Generic;
using V8.Domain.Entities;
using V8.Infra.Data.Context;

namespace V8.Infra.Data.EmployeeHistoryCreate
{
    public class CreateData
    {
        public static void Execute(SqlServerContext context)
        {
            IList<EmployeeHistory> history = new List<EmployeeHistory>();
            int SalaryId = 1;
            int EmployeeId = 1;

            while (SalaryId <= 100)
            {
                for (int i = 0; i < 10; i++)
                {
                    var employeeHistory = new EmployeeHistory()
                    {
                        Grade = Random(0,10),
                        Month = i + 1,
                        Year = 2020,
                        BeginDate = DateTime.Parse("2020-01-01T20:46:13.921Z"), 
                        EmployeeId = EmployeeId,
                        SalaryId = SalaryId,
                        OfficeId = Random(1,5),
                        DivisionId = Random(1,4),
                        PositionId = Random(1,8)
                    };

                    history.Add(employeeHistory);

                    SalaryId = SalaryId + 1;
                }

                EmployeeId = EmployeeId + 1;
            }

            foreach (EmployeeHistory e in history)
            {
                context.Set<EmployeeHistory>().Add(e);
            }

            context.SaveChanges();
        }

        public static int Random(int start, int end)
        {
            var rand = new Random();
            return ((int)(rand.NextDouble() * Math.Abs(end - start)) + start);
        }
    }
    
}