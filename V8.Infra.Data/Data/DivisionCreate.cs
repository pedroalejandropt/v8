﻿using System;
using V8.Domain.Entities;
using V8.Infra.Data.Context;

namespace V8.Infra.Data.DivisionCreate
{
    public class CreateData
    {
        public static void Execute(SqlServerContext context)
        {
            var divisions = new Division[]
            {
                new Division()
                {
                    Name = "Operation",
                    Description = ""
                },
                new Division()
                {
                    Name = "Sales",
                    Description = ""
                },
                new Division()
                {
                    Name = "Marketing",
                    Description = ""
                },
                new Division()
                {
                    Name = "Customer Care",
                    Description = ""
                }
            };

            foreach (Division e in divisions)
            {
                context.Set<Division>().Add(e);
            }

            context.SaveChanges();
        }
    }
}