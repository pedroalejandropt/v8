﻿using System;
using V8.Domain.Entities;
using V8.Infra.Data.Context;

namespace V8.Infra.Data.EmployeeCreate
{
    public class CreateData
    {
        public static void Execute(SqlServerContext context)
        {
            var employees = new Employee[]
            {
                new Employee()
                {
                    IdentificationNumber = "45642132",
                    EmployeeCode = "10001222",
                    EmployeeName = "Mike",
                    EmployeeSurname = "James",
                    Birthday = DateTime.Parse("1997-06-08T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "14598756",
                    EmployeeCode = "1003001",
                    EmployeeName = "Kali",
                    EmployeeSurname = "Prasad",
                    Birthday = DateTime.Parse("1983-11-18T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "34393823",
                    EmployeeCode = "10040012",
                    EmployeeName = "Jane",
                    EmployeeSurname = "Doe",
                    Birthday = DateTime.Parse("2000-03-24T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "23474829",
                    EmployeeCode = "10001333",
                    EmployeeName = "Mark",
                    EmployeeSurname = "Dol",
                    Birthday = DateTime.Parse("1990-08-02T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "34243434",
                    EmployeeCode = "10001444",
                    EmployeeName = "Elisabeth",
                    EmployeeSurname = "ODS",
                    Birthday = DateTime.Parse("1979-03-28T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "89734897",
                    EmployeeCode = "10001555",
                    EmployeeName = "Selena",
                    EmployeeSurname = "Gomez",
                    Birthday = DateTime.Parse("1993-01-31T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "23423466",
                    EmployeeCode = "10001666",
                    EmployeeName = "Justin",
                    EmployeeSurname = "B",
                    Birthday = DateTime.Parse("1989-10-12T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "45152323",
                    EmployeeCode = "10001777",
                    EmployeeName = "Zack",
                    EmployeeSurname = "Cody",
                    Birthday = DateTime.Parse("2001-07-09T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "45365452",
                    EmployeeCode = "10001988",
                    EmployeeName = "Miley",
                    EmployeeSurname = "Cyrus",
                    Birthday = DateTime.Parse("1977-10-03T20:46:13.921Z")
                },
                new Employee()
                {
                    IdentificationNumber = "63538362",
                    EmployeeCode = "10001989",
                    EmployeeName = "Martin",
                    EmployeeSurname = "Harris",
                    Birthday = DateTime.Parse("1997-04-03T20:46:13.921Z")
                }
            };

            foreach (Employee e in employees)
            {
                context.Set<Employee>().Add(e);
            }

            context.SaveChanges();
        }
    }
}
