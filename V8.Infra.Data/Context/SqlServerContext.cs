﻿using V8.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using V8.Infra.Data.Mapping;

namespace V8.Infra.Data.Context
{
    public class SqlServerContext : DbContext
    {
        public DbSet<ExampleEntity> Example { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<EmployeeHistory> EmployeeHistory { get; set; }
        public DbSet<Salary> Salary { get; set; }
        public DbSet<Office> Office { get; set; }
        public DbSet<Division> Division { get; set; }
        public DbSet<Position> Position { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            { 
                optionsBuilder.UseSqlServer("Data Source=127.0.0.1;Initial Catalog=v8db;Integrated Security=False;User ID=sa;Password=Pw123456*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ExampleEntity>(new ExampleMap().Configure);
            modelBuilder.Entity<Employee>(new EmployeeMap().Configure);
            modelBuilder.Entity<Salary>(new SalaryMap().Configure);
            modelBuilder.Entity<Office>(new OfficeMap().Configure);
            modelBuilder.Entity<Division>(new DivisionMap().Configure);
            modelBuilder.Entity<Position>(new PositionMap().Configure);
            modelBuilder.Entity<EmployeeHistory>(new EmployeeHistoryMap().Configure);
        }

    }
}
