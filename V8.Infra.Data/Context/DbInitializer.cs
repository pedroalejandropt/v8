﻿using System;
using V8.Infra.Data;

namespace V8.Infra.Data.Context
{
    public static class DbInitializer
    {
        public static void Initialize(SqlServerContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            EmployeeCreate.CreateData.Execute(context);
            SalaryCreate.CreateData.Execute(context);
            OfficeCreate.CreateData.Execute(context);
            DivisionCreate.CreateData.Execute(context);
            PositionCreate.CreateData.Execute(context);
            EmployeeHistoryCreate.CreateData.Execute(context);
        }
    }
}